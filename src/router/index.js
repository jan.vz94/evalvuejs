import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import MovieDetail from '../components/MovieDetail.vue'
import MovieEdit from '../components/MovieEdit.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'home',
    component: HomeView
}, {
    path: '/movie/:id',
    name: 'detail',
    component: MovieDetail,
    props: true,
}, {
    path: '/movie/edit/:id',
    name: 'edit',
    component: MovieEdit,
    props: true,
}, ]

const router = new VueRouter({
    routes
})

export default router